#include <iostream>
#include <string>
#include <conio.h>
#include <fstream>

using namespace std;

class Mahasiswa {
	public:
		string nama;
		string NIM;
		string jurusan;

		Mahasiswa(string nama, string NIM, string jurusan) {
			Mahasiswa::nama = nama;
			Mahasiswa::NIM = NIM;
			Mahasiswa::jurusan = jurusan;
		}

		string stringify() {
			return "\n" + Mahasiswa::nama + " " + Mahasiswa::NIM + " " + Mahasiswa::jurusan;
		}
};

class DBase {
	public:
		ifstream in; 
		ofstream out; 
		string fileName; // variabel untuk menuliskan nama file

		DBase(const char* fileName) {
			DBase::fileName = fileName;
		}

		void save(Mahasiswa data) {
			cout << data.nama << endl;
			cout << data.NIM << endl;
			cout << data.jurusan << endl;

			// buka file database, dengan kondisi flag append(tambah)
			DBase::out.open(DBase::fileName, ios::app);
			// tambahkan data
			DBase::out << data.stringify();
			// tutup file database
			DBase::out.close();
		}

		void showAll() {
			// buka file database, dengan kondisi flag in()
			DBase::in.open(DBase::fileName, ios::in);
			string nama, NIM, jurusan;
			int index;

			// baca seluruh data stream sampai habis
			while(!DBase::in.eof()) {
				DBase::in >> nama;
				DBase::in >> NIM;
				DBase::in >> jurusan;
				cout << index++ << "\t";
				cout << nama << "\t";
				cout << NIM << "\t";
				cout << jurusan << endl;
			}

			// tutup file database
			DBase::in.close();


		}

};

int main(int argc, char const *argv[]) {
	// membuat object-obejct yang diperlukan 
	DBase database = DBase("data.txt");


	// tampilkan seluruh data yang ada didalam database
	database.showAll();

	// input user
	string nama, NIM, jurusan;

	cout << "Masukan Data Mahasiswa" << endl;
	cout << "Nama: ";
	cin >> nama;
	cout << "NIM: ";
	cin >> NIM;
	cout << "Jurusan: ";
	cin >> jurusan;

	Mahasiswa dataMahasiswa = Mahasiswa(nama, NIM, jurusan);

	// save data ke database
	database.save(dataMahasiswa);

	getch();
	return 0;
}

