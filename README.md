# Latihan Membuat Database Dengan Paradigma OOP

+ ofsteam: Merupakan sebuah tipe data output yang memungkinkan kita untuk melakukan pembuatan dan menulis informasi pada file.
+ ifstream: Merupakan sebuah tipe data input yang memungkinkan kita melakukan pembacaan informasi dari file.
+ fstream: Merupakan sebuah tipe data input output file yang memungkinkan kita untuk melakukan kedua hal tersebut, seperti membuat, menulis dan membaca informasi dari file.


## Di bawah ini adalah macam-macam flag yang dapat digunakan untuk mendefinisikan mode pada operasi sebuah file:
+ ios::in	Untuk mendefinisikan pembukaan file sebagai sebuah operasi pembacaan (input)
+ ios::out	Untuk mendefinisikan pembukaan file sebagai sebuah operasi penulisan (output)
+ ios::binary	membuka file dalam mode binari
+ ios::ate	Membuka sebuah file dan memindahkan kendali input dan output pada akhir dari file, jika tidak maka kendali akan berada pada awal file.
+ ios::app	Mode penambahan, setiap penambahan tulisan akan ditambahkan pada akhir file.
+ ios::trunc	Jika file sudah ada, maka akan digantikan sepenuhnya sebelum proses membuka file.




